# Copyright 2022 Ian Jackson and contributors to dkim-rotate
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

TESTS=$(notdir $(wildcard tests/t-*[^~]))

MANPAGES=dkim-rotate.1 dkim-rotate.5 dkim-rotate.7
HTML_DOCS=README.html INTERNALS.html $(addsuffix .html,$(MANPAGES))
TARGETS=$(HTML_DOCS) $(MANPAGES) 

INSTALL ?= install
SHELL = /bin/bash

PUBLISH_DOCS_DEST_HOST=ianmdlvl@login.chiark.greenend.org.uk
PUBLISH_DOCS_DEST_DIR=public-html/dkim-rotate/
PUBLISH_DOCS_DEST=$(PUBLISH_DOCS_DEST_HOST):$(PUBLISH_DOCS_DEST_DIR)

all: $(TARGETS)

prefix		?= /usr
etc_dir		?= /etc/dkim-rotate
bin_dir		?= $(prefix)/bin
man_dir		?= $(prefix)/share/man
doc_dir		?= $(prefix)/share/doc/dkim-rotate
examples_dir	?= $(doc_dir)/examples
var_dir		?= /var/lib/dkim-rotate

i=&& mv $@.tmp $@
o=>$@.tmp $i

install: all
	$(INSTALL) -d $(DESTDIR){$(etc_dir),$(bin_dir),$(var_dir)}
	$(INSTALL) -d $(DESTDIR){$(doc_dir),$(man_dir),$(examples_dir)}
	$(INSTALL) -m 755 dkim-rotate $(DESTDIR)$(bin_dir)/.
	$(INSTALL) -m 644 $(HTML_DOCS) $(DESTDIR)$(doc_dir)/.
	$(INSTALL) -m 644 crontab example.zone $(DESTDIR)$(examples_dir)/.
	set -e; for m in $(MANPAGES); do 			\
		md=$(DESTDIR)$(man_dir)/man$${m##*.}; 		\
		$(INSTALL) -d $$md; $(INSTALL) -m 644 $$m $$md;	\
		done

dkim-rotate%: dkim-rotate%.md
	pandoc -tman --standalone $< $o

%.html: %.md
	pandoc --standalone $< $o

publish-docs: $(HTML_DOCS)
	rsync $(HTML_DOCS) $(PUBLISH_DOCS_DEST)/.
	ssh $(PUBLISH_DOCS_DEST_HOST) \
 'set -e; cd $(PUBLISH_DOCS_DEST_DIR); ln -s README.html index.html.tmp; mv -f index.html.tmp index.html'

check: $(foreach T,$(TESTS),tmp/$T.ok)

tmp/%.ok: tests/% tests/run1 tests/lib dkim-rotate
	tests/run1 $* $<

clean:
	rm -rf tmp $(TARGETS)
