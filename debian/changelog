dkim-rotate (1.1) unstable; urgency=medium

  Important bugfix:
  * Fix reload failure handling with multiple reload-neededs.
    Closes: #1064452.  [Report from Daniel Gröber]

  Documentation etc.:
  * Correct spelling mistakes in docs etc.  [MR !1 from Edward Betts]
  * example.zone: Clarify commented directive.  [Report from Daniel Gröber]
  * dkim-rotate(5): Add a SEE ALSO referencing the example config.
  * debian/control: Improve description.

  Tests:
  * tests: Test spurious "corrupted state" bug #1064452.

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Sun, 25 Feb 2024 01:32:47 +0000

dkim-rotate (1.0) unstable; urgency=medium

  Documentation fixes:
  * dkim-rotate(5): Fix default for `dkim_txt` to be accurate.
  * Fix some typos and a formatting botch.

  * Declare 1.0.

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Mon, 14 Aug 2023 19:11:16 +0100

dkim-rotate (0.4) unstable; urgency=medium

  Functional changes:
  * Adjust default/example crontab.
  * Reload the MTA properly after key generaion in more cases.

  Documentation:
  * Greatly expand and reorganise documentation.

  Packaging and build system:
  * Dependency management somewhat automated and improved.
    (Dependency on perl is now on perl:any.)
  * Machinery for publishing docs to website.
  * Removed spurious fourth number from Standards-Version.
    [Report from Sean Whitton.]

  Tests:
  * Introduced run1 wrapper to get code out of Makefile.
  * Check that we don't have unexpected rotation stalls.
  * New `cparams` test for running twice daily with many selectors.
  * Add autopkgtests.

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Sun, 07 Aug 2022 20:10:17 +0100

dkim-rotate (0.3) unstable; urgency=medium

  * Bump version for actual initial upload to Debian.

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Sat, 06 Aug 2022 17:16:56 +0100

dkim-rotate (0.2) unstable; urgency=medium

  * WIP, tidying up for initial upload to Debian.

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Sat, 06 Aug 2022 16:57:11 +0100

dkim-rotate (0.1) unstable; urgency=medium

  * WIP, initial version for testing on chiark.

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Mon, 01 Aug 2022 12:15:50 +0100

# Copyright 2022 Ian Jackson and contributors to dkim-rotate
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.
