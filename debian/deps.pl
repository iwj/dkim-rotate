
sub scan_extract () {
  my %o;
  my $y = '';
  open D, "debian/control" or die $!;
  while (<D>) {
    if (m{^\S}) {
      $y = m{^\# \s* \@\@\@ \s* (\w+) \s+ (\w+) \s*$}x ? "$1:$2" : '';
      next;
    }
    next unless $y;
    die unless m{^\s*(.*,)\s*$};
    $o{$y} .= ' ' if length $o;
    $o{$y} .= $1;
  }
  D->error and die $!;
  return %o;
}

1;
