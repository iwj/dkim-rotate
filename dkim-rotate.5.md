% dkim-rotate(5)

NAME
====

`/etc/dkim-rotate/`_instance_`.zone` - `dkim-rotate` configuration file

INTRODUCTION
============

The config file for each instance is `/etc/dkim-rotate/`_instance_`.zone`
for any _instance_.
(When scanning for instances,
only ones matching `[a-z][-_0-9a-z]*`
are considered.)

Conventionally,
the usual default instance is `/etc/dkim-rotate/dkim.conf`.

Each configuration file is,
broadly speaking,
in DNS zonefile syntax.

The config file should contain a `SOA`, suitable `NS` records,
and any other records thought desirable.

The string `;!SERIAL` must appear, immediately after some digits.
It should appear once, in the `SOA`.
`dkim-rotate` will replace the digits
with a suitable serial number.
The config file value is also used as the starting point for a new instance.

dkim-rotate will make a DNS zone out of this file,
adding an DKIM `TXT` RR for each selector.
(Directly in the zone,
i.e., the generated zonefile will contain for example `f TXT ...`).

The zonefile's prevailing TTL applies to the generated RRS,
and can therefore be adjusted using the nameserver's `$TTL` feature.

The config file should *not* normally contain `MX` records,
because the `dkim-rotate`-managed zonefile
should usually be a different zone to your principal mail domain zone(s).
(See the Principles of Operation in dkim-rotate(7).)

`$ORIGIN` may be used to cause the `dkim-rotate`-generated RRs
to appear as sub-labels of a sub-domain.

The config file also contains some special directives,
on lines starting with `!`,
which contain the configuration for dkim-rotate.
These will be stripped out so that they don't appear
in the output zonefile gneerated by dkim-rotate.

The parameters for an existing instance may safely be adjusted.
(Although, reducing `max_selector` does not take full effect immediately.)

IMPORTANT DIRECTIVES
====================

`! pub_url` _url-string_

:   URL at which our published private keys are available.
    This should be a public URL
    at which the directory _instance_var_dir_`/pub/` is accessible.

    This is used:

      * For a `n=` field in the generated DKIM TXT RRs.
      * For the `header_note` and various `url` outputs in the MTA config.
      * To check that a key we are trying to reveal is actually revealed.

    A warning will be generated if a supposedly-revealed key is found not
    to be accessible.

    `-` can be used to indicate "none".
    This will also suppress the corresponding note
    in the generated TXT RRs,
    and the informational outputs in the MTA configuration.

`! mta_group` _identifier_

    Group that ought to be able to access DKIM private keys
    This should be a group which the MTA will have
    when it wishes to make a signature.

    For an Exim installation on Debian, `Debian-exim` is correct.
    
    This is used for the group ownership of
     `/var/lib/dkim-rotate/INSTANCE/priv`.

    The permissions `0750` will be used
    if dkim-rotate needs to create the directory.
    (Otherwise dkim-rotate will just check that the directory
    doesn't have global execute.)

    If `mta_group` is set to `-`,
    dkim-rotate will skip the chgrp,
    and (if it is creating the directory) create as `0700`.

    (Within the `priv/` directory,
    the files have a world-readable mode,
    so it is the directory permission which controls access.)

TUNING DIRECTIVES
=================

`! max_selector` _selector_

:   Indicates number of `CNAME`s (selectors) to use.
    _selector_ should be
    a lowercase letter
    (indicating that `a` to _selector_ inclusive can be used)
    or a positive integer (incidating that that many should be used).
    (If this number is revised downwards after deployment,
    it may take a little while for the extra names to stop being used.)

    Default is `12` (or, equivalently, `l`).

    If the configuration for an existing instance is changed,
    to reduce `max_selector`,
    it will take a little while for `dkim-rotate`
    to stop using of the to-be-abolished selectors:

    While the number of selectors in use is more than configured,
    `dkim-rotate` will print warnings.
    `dkim-rotate --status` can be used to verify whether
    old selectors are no longer in use,
    and therefore whether it is OK to remove associated DNS aliases etc.

`! dns_lag` _duration_

:   Time after successful installation of new zonefile,
    and nameserver reload,
    after which the new DNS entries are expected to be visible to all clients.

    _duration_ is a number (possibly with decimal fraction)
    followed by one of the units `s` `m` `h` `d` `w`.

    Default is `4h`.

`! email_lag` _duration_

:   Time after successful installation of new MTA configuration,
    and config reload,
    after which we will assume that all messages
    signed with the key indicated by the previous configuration
    have been delivered, or bounced.

    Default is `88h`.

`! instance_var_dir` _directory_

:   Where to store statefiles, MTA configuration, zone files, etc.
    Default is `/var/lib/dkim-rotate/`_instance_.

    If _instance_ specified on the command line as a pathname,
    there is no default for `instance_var_dir`:
    it must then be specified in the config file.

`! dkim_txt` _tag list_

    The values for the `TXT` RR, in domainkeys tag-list format,
    not including the `p=`.
    (see [RFC6376](https://datatracker.ietf.org/doc/html/rfc6376#section-3.6.1)).

    dkim-rotate will append `; n=...; p=...` to this.
    Default is `v=DKIM1; h=sha256; s=email`.

`! rsa_bits` _integer_

    Number of bits in generated RSA keys.
    (Currently, only RSA is supported.)
    Defaults to 2048.

`! dns_reload` _shell command_

    Command to run to reload the nameserver,
    after the zonefile has been updated.
    Default: `rndc reload >/dev/null`.

`! mta_reload` _shell command_

    Command to run to reload the MTA,
    after the DKIM signing configuration has been updated.
    Default: `true` (meaning, nothing needs to be done).

AUTHOR
======

| Copyright 2022 Ian Jackson and contributors to dkim-rotate.
| There is NO WARRANTY.
| `SPDX-License-Identifier: GPL-3.0-or-later`

SEE ALSO
========

`/usr/share/doc/dkim-rotate/examples/example.zone`

:   Very minimal example configuration.

`/usr/share/doc/dkim-rotate/examples/crontab`

:   Example configuration for cron.

dkim-rotate(7)

:   Principles of Operation

dkim-rotate(1)

:   Command line reference

RFC6376

:   DKIM Signatures

BIND9 Zone File format

:   <https://downloads.isc.org/isc/bind9/9.18.5/doc/arm/html/chapter3.html#soa-rr>
