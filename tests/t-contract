#!/bin/bash
# Copyright 2022 Ian Jackson and contributors to dkim-rotate
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

set -ex

. tests/lib

day 0 --new

for day in {1..9}; do
day $day --major
done

cat >>$tmp/etc/test.zone <<END -
! max_selector 2
END

invoke 10 +0 --major

diff -ub - $tmp/out.10.+0 <<END
test                 (h) +X  R revealed.
test                  g  +N    deadvertise? not until T
dkim-rotate: instance test: warning: cannot yet adjust selector count: offset 2 != 0
test                  d  +0 +N advanced; now emails percolating.
test                  c  -1 +0 advanced; now in use.
test                  b     -1 generated.
END

diff -ub - $tmp/out.10.+0.status <<END
test                  b  -1  generated   since T
test                  c  +0  active      
test                 ----- scheduled for abolition: -----
test                  d  +N  percolating since T
test                  e  +N  percolating since T
test                  f  +N  percolating since T
test                  g  +N  percolating since T
test                  h      free
test                  i      free
test                  j      free
test                  k      free
test                  l      free
test                  a      free
END

day 11

diff -ub - $tmp/out.11.+0 <<END
test                  -  +X    reveal?      no key
test                  g  +N +X deadvertised.
test                  f  +N    deadvertise? not until T
dkim-rotate: instance test: warning: cannot yet adjust selector count: offset 1 != 0
test                  c  +0 +N advanced; now emails percolating.
test                  b  -1 +0 advanced; now in use.
test                  a     -1 generated.
END

diff -ub - $tmp/out.11.+18 <<END
test                 (g) +X  R revealed.
test                  f  +N +X deadvertised.
test                  e  +N    deadvertise? not until T
dkim-rotate: instance test: warning: cannot yet adjust selector count: 5 keys, want 2
test                  a  -1    advance/use? not --major run
test                  a  -1    generate?    already exists.
END

day 12
day 13

diff -ub - $tmp/out.13.+18 <<END
test                  -  +X    reveal?      no key
test                  d  +N +X deadvertised.
test                  c  +N    deadvertise? not until T
dkim-rotate: instance test: warning: cannot yet adjust selector count: 3 keys, want 2
test                  -  -1    advance/use? no key
test                  -  -1    generate?    selector abolition (3 >= 2)
END

day 14

diff -ub - $tmp/out.14.+0.done <<END
test                 (d) +X  R revealed.
dkim-rotate: instance test: warning: cannot yet adjust selector count: 3 keys, want 2
END

diff -ub - $tmp/out.14.+18 <<END
test                  -  +X    reveal?      no key
test                  c  +N +X deadvertised.
test                  b  +N    deadvertise? not until T
dkim-rotate: instance test: selector limit adjusted to 2
test                  -  -1    advance/use? no key
test                  -  -1    generate?    no available selector (2 >= 2)
END

diff -ub - $tmp/out.14.+18.status <<END
test                  a  +0  active      
test                  b  +N  percolating since T
test                 (c) +X  revoking    since T
END

invoke 15 +0 --major

diff -ub - $tmp/out.15.+0 <<END
test                 (c) +X  R revealed.
test                  b  +N    deadvertise? not until T
test                  -  -1    advance/use? no key
test                  -  -1    generate?    no available selector (2 >= 2)
END

diff -ub - $tmp/out.15.+0.status <<END
test                  a  +0  active
test                  b  +N  percolating since T
END

echo ok.
