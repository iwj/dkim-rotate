#!/bin/bash
# Copyright 2022 Ian Jackson and contributors to dkim-rotate
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

set -ex

. tests/lib

day 0 --new
day 1

egrep . $tmp/var/{exim,zone}
find $tmp/var/ -type f -ls

exim_param () {
    local k=$1
    local v=$(sed -n "s/^$k: //p" $tmp/var/exim)
    eval "$k=\$v"
}

exim_param selector
exim_param readme_url
exim_param key_reveal_url

pubdata=$(perl <$tmp/var/zone -ne '
	next unless s{^'"$selector"' \s+ in \s+ txt \s+ }{}xi;
	die unless s{^"}{};
	die unless s{"$}{};
	s{"\s*"}{}g;
	s{\\(\d{3})}{ chr oct $& }ge;
	s{.*; p=}{} or die;
	print or die;
	$y = 1;
	END { $y or die }
')

rune=$(curl "$readme_url" | perl -ne '
  next unless m{^Calculating.*rune like this:$}...m{^\S};
  next unless m{^\s+\S};
  s{^\s+echo '\''[^'\''|]+'\''\s+\|}{} or die;
  print or die;
  $y = 1;
  END { $y or die }
')

for d in 2 6; do day $d; done
find $tmp/var/ -type f -ls

check_url () {
    curl "$1" >/dev/null
}

# check URL from recipe in readme
leaf=$(printf "%s\\n" "$pubdata" | eval "$rune")

check_url "${readme_url%/*}"/$leaf
check_url "$key_reveal_url"

echo ok.
